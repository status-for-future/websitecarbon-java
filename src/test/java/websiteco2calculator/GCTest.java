package websiteco2calculator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.lang.InterruptedException;
import org.junit.jupiter.api.Timeout;
import java.util.concurrent.TimeUnit;
import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

class GCTest {

    private static final long maxTimeMillis=20;

    @Test
    void test1() throws InterruptedException {
        long start = System.currentTimeMillis();
        businessLogic();
        long end = System.currentTimeMillis();


        assertTrue(
            (end-start) < maxTimeMillis
            ,String.format("execution took %d millis. that's more than %d millis.", end-start, maxTimeMillis)
        );
    }
    
    @Test
    @Timeout(value=maxTimeMillis, unit=TimeUnit.MILLISECONDS)
    void test2() throws InterruptedException {
        businessLogic();
    }


    @Test
    void test3() throws InterruptedException {
        assertTimeout(
            Duration.ofMillis(maxTimeMillis), 
            () -> businessLogic()
        );
    }

    private void businessLogic() throws InterruptedException {
        Thread.sleep(10);
    }
}
