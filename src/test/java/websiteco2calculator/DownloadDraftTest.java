package websiteco2calculator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

// This "test" was used to draft some new functionality that is not ready yet.
// I want to download a webpage from a local webserver to calculate CO2 consumption
// for it
public class DownloadDraftTest {

	void foo() throws IOException {

		URL url = new URL("http://localhost:8000/");
		File download = new File("download/" + System.currentTimeMillis());
		if (!download.exists() || !download.isDirectory()) {
			download.mkdirs();
		}
		Set<String> nextDownloads = download(url, download);

		nextDownloads.stream().forEach(lnk -> {
			try {
				System.out.println(lnk);
				download(new URL(lnk), download);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});

		if (download.isDirectory()) {
			Long directorySize = Arrays.asList(download.listFiles()).stream()
					.collect(Collectors.summingLong((file) -> file.length()));
			System.out.println(WebPage.withPageSize(Bytes.B(directorySize)).calculateCO2());
			System.out.println(readableFileSize(directorySize));
		}
	}

	public static String readableFileSize(long size) {
		if (size <= 1)
			return size + " byte";

		final String[] units = new String[] { "bytes", "KB", "MB", "GB", "TB", "PB", "EB" };
		int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
		return new DecimalFormat("#,##0.##").format(size / Math.pow(1024, digitGroups))
				+ " " + units[digitGroups];
	}

	private Set<String> download(URL url, File download) throws IOException {

		File newFile = new File(url.getFile());
		String targetFilename = newFile.getName();
		if (targetFilename.isEmpty()) {
			targetFilename = "root";
		}
		File pageFile = new File(download, targetFilename);

		URL actualURL = followRedirects(url);
		Set<String> nextDownloads = new HashSet<>();

		HttpURLConnection con = (HttpURLConnection)(actualURL.openConnection());
		try (InputStream openStream = actualURL.openStream();
			 InputStreamReader inputStreamReader = new InputStreamReader(openStream);
			 BufferedReader reader = new BufferedReader(inputStreamReader);
			 FileWriter fileWriter = new FileWriter(pageFile);
			 BufferedWriter writer = new BufferedWriter(fileWriter);) {
			String line;

			con.connect();
			String contentlength= con.getHeaderField("content-length");
			System.out.println(contentlength);

			while ((line = reader.readLine()) != null) {
				writer.write(line);
				if (line.matches(".* src=[\"'].*")) {
					String[] splitted = line.split("src=");
					Set<String> nextDLs = Arrays.asList(splitted).stream()
							.skip(1)
							.map(l -> l.replaceFirst("[\"']([^\"']+)[\"'].*", "$1"))
							.map(lnk -> {
								if (!lnk.startsWith("http")) {
									return actualURL.toExternalForm() + lnk;
								}
								return lnk;
							})
							.collect(Collectors.toSet());
					nextDownloads.addAll(nextDLs);
				}
			}
			return nextDownloads;
		} catch (IOException e ) {
			throw new RuntimeException(e);
		}
	}

	private URL followRedirects(URL url) {
		URLConnection urlopen = null;
		try {
			urlopen = url.openConnection();
		} catch (IOException e) {
			e.printStackTrace();
			return url;
		}
		HttpURLConnection con = (HttpURLConnection)(urlopen);
		try {
			con.setInstanceFollowRedirects( false );
			con.connect();
			int responseCode = con.getResponseCode();
			if (responseCode >299 && responseCode<400) {
				System.out.println(responseCode);
				String location = con.getHeaderField("Location");
				System.out.println(location);
				return new URL(location);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			con.disconnect();
		}
		return url;
	}

}
