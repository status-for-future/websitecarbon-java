package websiteco2calculator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class BytesTest {

    @Test
    void create_parseDouble() {
        assertNotNull(Bytes.B(42.0));
    }

    @Test
    void create_parseLong() {
        assertNotNull(Bytes.B((long) 12));
    }

    @ParameterizedTest
    @ValueSource(longs = {0, 1, 2, 52, 498265231})
    void validLongValues(long value) {
        assertEquals(value, Bytes.B(value).asLong());
    }

    @ParameterizedTest
    @ValueSource(longs = {-1, -2, -2398475})
    void invalidLongValues(long value) {
        assertThrows(IllegalArgumentException.class, () -> Bytes.B(value));
    }

    @ParameterizedTest
    @ValueSource(doubles = {0.01, 1.0, 2.0, 52.9, 498265231.0, -0.5})
    void validDoublealues(double value) {
        assertEquals(Math.round(value), Bytes.B(value).asLong());
    }

    @ParameterizedTest
    @ValueSource(doubles = {-0.501, -2.0})
    void invalidDoubleValues(double value) {
        assertThrows(IllegalArgumentException.class, () -> Bytes.B(value), "expected " + value + " to be invalid.");
    }


    @Test
    void energyConsumption() {
        assertNotNull(Bytes.B((long) 42).toEnergyConsumption());
    }

    @ParameterizedTest
    @CsvSource({
            "0, 0"
            , "1, 1.6810372471809387E-9"
            , "1000, 1.6810372471809387E-6"
            , "1000000, 1.6810372471809387E-3"
            , "1000000000, 1.6810372471809387"
            , "2000000000, 3.3620744943618774"
    })
    void energyConsumption(long in, double expected) {
        assertEquals(expected, Bytes.B(in).toEnergyConsumption().asDouble());
    }


    @Test
    void kb() {
        assertEquals(512 + 2048, Bytes.KB(2.5).asLong());
    }


    @Test
    void mb() {
        int oneMB = 1024 * 1024;
        assertEquals(oneMB + oneMB / 2, Bytes.MB(1.5).asLong());
    }
}
