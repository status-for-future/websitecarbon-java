package websiteco2calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

public class KiloWattGrammPerByteTest {
	@Test
	void create() {
		assertNotNull(KiloWattGrammPerByte.of(Bytes.B(1)));
	}

	@Test
	void KWG_PER_GB() {
		assertEquals(1.805, KiloWattGrammPerByte.KWG_PER_GB);
	}

	@Test
	void toCO2PerKWg() {
		assertNotNull(KiloWattGrammPerByte.of(1).toCO2PerKWg());
	}

}
