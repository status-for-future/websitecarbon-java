package websiteco2calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import org.junit.jupiter.api.Test;

public class PercentileFromFileTest {

	@Test
	void readFromTextFileInClasspath() {
		InputStream stream = PercentileFromFileTest.class.getResourceAsStream("/2020-01-27.json");
		
		assertNotNull(stream, "stream");
	}

	@Test
	void convertToJson() {
		InputStream stream = PercentileFromFileTest.class.getResourceAsStream("/percentileFromFileTest.json");

		List result = new Gson().fromJson(new InputStreamReader(stream), List.class);

		assertNotNull(result, "result");
		assertEquals(2, result.size());
		assertEquals(8.0, result.get(0));
		assertEquals(9.0, result.get(1));
	}	
}
