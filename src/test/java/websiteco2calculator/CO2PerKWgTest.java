package websiteco2calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class CO2PerKWgTest {
	@Test
	void create() {
		assertNotNull(CO2PerKWg.of(KiloWattGrammPerByte.of(1)));
	}

	@Test
	void CARBON_PER_KWG_GRID() {
		assertEquals(477.0, CO2PerKWg.CO2_PER_KWG_GRID);
	}

	@Test
	void CARBON_PER_KWG_RENEWABLE() {
		assertEquals(33.4, CO2PerKWg.CO2_PER_KWG_RENEWABLE);
	}

	@Test
	void defaultEnergyInDataCenter() {
		assertEquals(Percentage.of(0.1008), CO2PerKWg.ENERGY_IN_DATACENTER);
	}

	@Test
	void defaultEnergyInTransmissionAndEndUser() {
		assertEquals(Percentage.of(0.8992), CO2PerKWg.ENERGY_IN_TRANSMISSION_AND_END_USER);
	}

	@ParameterizedTest
	@ValueSource(ints = { 0, 1, 42, 65489521})
	void grid(int input) {
		double expected = input * CO2PerKWg.CO2_PER_KWG_GRID;
		assertEquals(expected, CO2PerKWg.of(input).getGrid().asDouble());
	}


	@ParameterizedTest
	@ValueSource(ints = { 0, 1, 42, 65489521})
	void renewable(int input) {
		double expected = (input * CO2PerKWg.ENERGY_IN_DATACENTER.asDouble() * CO2PerKWg.CO2_PER_KWG_RENEWABLE) + 
			(input * CO2PerKWg.ENERGY_IN_TRANSMISSION_AND_END_USER.asDouble() * CO2PerKWg.CO2_PER_KWG_GRID);
		assertEquals(expected, CO2PerKWg.of(input).getCO2Renewable().asDouble());
	}

}
