package websiteco2calculator;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class CliTest {

    private static final String NEWLINE = System.getProperty("line.separator");
    private static final String[] NO_ARGUMENTS_NECESSARY = new String[0];
    OutputStreamExtension foo;
    ByteArrayOutputStream mockedSystemOut = new ByteArrayOutputStream();
    ByteArrayOutputStream mockedSystemErr = new ByteArrayOutputStream();
    private PrintStream outPrintStream;
    private PrintStream errorPrintStream;

    @BeforeEach
    void beforeEach() {
        foo = new OutputStreamExtension();
        outPrintStream = new PrintStream(foo);
        System.setOut(outPrintStream);

        errorPrintStream = new PrintStream(mockedSystemErr);
        System.setErr(errorPrintStream);
    }

    @AfterEach
    void afterEach() {
        outPrintStream.close();
        errorPrintStream.close();
    }

    @Test
    void askForInput() {
        setSystemInWithInputLines("irrelevant input");

        Cli.main(NO_ARGUMENTS_NECESSARY);

        assertSystemOutContainsText("Enter \"q\" to quit.");
        assertSystemOutContainsText("Please enter pageSize in MB:");
    }

    @Test
    void quitOnQ() {
        setSystemInWithInputLines("q");

        Cli.main(NO_ARGUMENTS_NECESSARY);

        assertSystemOutContainsText("Quitting program. Bye.");
    }

    @Test
    void parseMultipleNumbers() {
        setSystemInWithInputLines("1", "2", "q");

        Cli.main(NO_ARGUMENTS_NECESSARY);

        assertSystemOutContainsTextTimes("pageSizeBytes", 2);
    }

    @Test
    void createBytesFromInput() {
        setSystemInWithInputLines("2");

        Cli.main(NO_ARGUMENTS_NECESSARY);

        assertEquals(Bytes.MB(2).toString(), Cli.inputPageSize.toString());
    }

    @Test
    void calculateWebPageStats() {
        setSystemInWithInputLines("2");

        Cli.main(NO_ARGUMENTS_NECESSARY);

        assertNotNull(Cli.webPageWithStatistics);
        assertNotNull(mockedSystemOut.toString());
    }

    @Test
    void testSystemErr() {
        System.err.println("bla");
        assertSystemErrContains("bla");
    }

    @Test
    void invalidInput() {
        setSystemInWithInputLines("foo");

        Cli.main(NO_ARGUMENTS_NECESSARY);

        assertSystemErrContains("Invalid input. Expected a number of MB");
    }

    private void assertSystemErrContains(String string) {
        assertEquals(string + NEWLINE, mockedSystemErr.toString());
    }

    private void assertSystemOutEqualsTextLine(String expectedText) {
        assertEquals(expectedText + NEWLINE, mockedSystemOut.toString());
    }

    private void assertSystemOutContainsText(String expectedText) {
        assertTrue(mockedSystemOut.toString().contains(expectedText),
                "Expected System.out to contain \"" + expectedText + "\" " +
                        "but didn't find it: \"" + mockedSystemOut.toString() + "\"");
    }

    private void assertSystemOutContainsTextTimes(String expectedText, int times) {
        String[] splitter = mockedSystemOut.toString().split(expectedText, 0);

        assertEquals(times, splitter.length - 1, "Expected System.out to contain \"" + expectedText + "\" " + times
                + " times " + "but only found it " + (splitter.length - 1) + " times.");
    }

    private void setSystemInWithInputLines(String... inputs) {
        String input = Arrays.stream(inputs).collect(Collectors.joining(NEWLINE));
        InputStream inputStream = inputStreamWith(input);
        System.setIn(inputStream);
    }

    private InputStream inputStreamWith(String input) {
        return new ByteArrayInputStream(input.getBytes());
    }

    private final class OutputStreamExtension extends OutputStream {

        private String line = "";

        @Override
        public void write(int arg0) throws IOException {
            mockedSystemOut.write(arg0);
            char[] chars = Character.toChars(arg0);
            if (chars.length > 0) {
                String valueOf = String.valueOf(chars);
                if (!valueOf.isEmpty()) {
                    line += valueOf;
                }
            }
        }

        String getLine() {
            return this.line;
        }

        @Override
        public String toString() {
            return mockedSystemOut.toString();
        }
    }
}
