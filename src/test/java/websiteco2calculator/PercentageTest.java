package websiteco2calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

class PercentageTest {

	@ParameterizedTest
	@ValueSource(ints = { 0,1,2,3,42,100 })
	void validValues(int value) {
		assertNotNull(Percentage.of(value));
	}


	@ParameterizedTest
	@ValueSource(ints = { -1, -12, 101 })
	void invalidValues(int value) {
		assertThrows(IllegalArgumentException.class, () -> Percentage.of(value));
	}

	@ParameterizedTest
	@ValueSource(doubles = { -1.0, -12.0, 100.001 })
	void invalidDoubleValues(double value) {
		assertThrows(IllegalArgumentException.class, () -> Percentage.of(value));
	}


	@ParameterizedTest
	@CsvSource({
		  "0, 0.0"
		, "1, 0.01"
		, "42, 0.42"
		, "100, 1.0"
	})
	void equals(int intvalue, double doublevalue) {
		assertEquals(intvalue, Percentage.of(doublevalue).asInt());
		assertEquals(doublevalue, Percentage.of(doublevalue).asDouble());
		assertEquals(Percentage.of(intvalue), Percentage.of(doublevalue), intvalue + " should be same as " + doublevalue);
	}
	
}
