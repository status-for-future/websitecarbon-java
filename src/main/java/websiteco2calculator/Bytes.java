package websiteco2calculator;

public final class Bytes {

    private final long value;
    private static final int BASE = 1024;

    private Bytes(long value) {
        if (value < 0) {
            throw new IllegalArgumentException("must be greater than 0");
        }
        this.value = value;
    }

    public long asLong() {
        return value;
    }

    private static Bytes parseInternal(double result) {
        return new Bytes(Math.round(result));
    }

    public KiloWattGrammPerByte toEnergyConsumption() {
        return KiloWattGrammPerByte.of(this);
    }

    public static Bytes MB(double d) {
        return parseInternal(d * Math.pow(BASE, 2));
    }

    public static Bytes KB(double d) {
        return parseInternal(d * Math.pow(BASE, 1));
    }

    public static Bytes B(double d) {
        return parseInternal(d);
    }

    @Override
    public String toString() {
        return "Bytes[" + this.value + "]";
    }
}
