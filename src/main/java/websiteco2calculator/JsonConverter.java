package websiteco2calculator;

public interface JsonConverter {

    String toJson(Object o);

}
