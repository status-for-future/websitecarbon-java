package websiteco2calculator;

public final class WebPage {

    static final Percentage RETURNING_VISITOR_PERCENTAGE = Percentage.of(75);
    static final Percentage DATA_LOADED_ON_SUBSEQUENT_LOAD = Percentage.of(2);
    static final Percentage FIRST_TIME_VIEWING_PERCENTAGE = Percentage.of(25);
    private final Bytes pageSizeBytes;

    private WebPage(Bytes bytes) {
        pageSizeBytes = bytes;
    }

    public static WebPage withPageSize(Bytes bytes) {
        if (null == bytes) {
            throw new IllegalArgumentException("pageSize must not be null");
        }
        return new WebPage(bytes);
    }

    public Bytes getAdjustedDataTransferRate() {
        Bytes input = this.pageSizeBytes;
        double result = (input.asLong() * RETURNING_VISITOR_PERCENTAGE.asDouble()) + (input.asLong() * DATA_LOADED_ON_SUBSEQUENT_LOAD.asDouble() * FIRST_TIME_VIEWING_PERCENTAGE.asDouble());

        return Bytes.B(result);
    }

    public WebPageWithStatistics calculateCO2() {
        KiloWattGrammPerByte energyConsumption = this.getAdjustedDataTransferRate().toEnergyConsumption();
        CO2PerKWg co2PerKWg = energyConsumption.toCO2PerKWg();
        CO2 renew = co2PerKWg.getCO2Renewable();
        CO2 grid = co2PerKWg.getGrid();
        return new WebPageWithStatistics(this.pageSizeBytes, energyConsumption, renew, grid);
    }

}
