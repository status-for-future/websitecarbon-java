package websiteco2calculator;

import static websiteco2calculator.CO2UsagePercentiles.getPercentiles;

public final class CO2 {

    public enum Type {
        GRID, RENEWABLE;
    }

    private final Type type;
    private final double value;
    private final Percentage cleanerThan;

	public CO2(double d, Percentage cleanerThan) {
		this(d, Type.GRID, cleanerThan);
	}

    public CO2(double d) {
        this(d, Type.GRID);
    }

	public CO2(double d, Type type) {
        this.value = d;
        this.type = type;
        this.cleanerThan = CO2UsagePercentiles.calculateCleanerThan(getPercentiles(), this.value);
    }

	public CO2(double d, Type type, Percentage cleanerThan) {
		this.value = d;
		this.type = type;
		this.cleanerThan = cleanerThan;
	}

    public Double asDouble() {
        return this.value;
    }

    public Percentage getCleanerThan() {
        return cleanerThan;
    }

    @Override
    public String toString() {
        return String.format("%.6f CO₂ (%s)", this.value, this.type.name().toLowerCase());
    }

}
