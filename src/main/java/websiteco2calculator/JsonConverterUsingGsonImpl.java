package websiteco2calculator;

import com.google.gson.Gson;

class JsonConverterUsingGsonImpl implements JsonConverter {

    @Override
    public String toJson(Object o) {
        return new Gson().toJson(o);
    }

}
