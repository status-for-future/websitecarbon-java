package websiteco2calculator;

import java.util.Scanner;

public class Cli {

    static Bytes inputPageSize;
    static WebPageWithStatistics webPageWithStatistics;

    public static void main(String[] args) {
        String text;
        try (Scanner scanner = new Scanner(System.in)) {
            while (true) {
                System.out.println("Enter \"q\" to quit.");
                System.out.println("Please enter pageSize in MB:");
                text = scanner.nextLine();
                if (text.equals("q")) {
                    System.out.println("Quitting program. Bye.");
                    return;
                }
                try {
                    inputPageSize = Bytes.MB(Double.parseDouble(text));
                    webPageWithStatistics = WebPage.withPageSize(inputPageSize).calculateCO2();
                    System.out.println(webPageWithStatistics);
                    System.err.println("cleaner than " + webPageWithStatistics.getCO2Grid().getCleanerThan());
                } catch (NumberFormatException e) {
                    System.err.println("Invalid input. Expected a number of MB");
                }
            }
        } catch (RuntimeException e) {
            // Do nothing atm.
            return;
        }
    }
}
