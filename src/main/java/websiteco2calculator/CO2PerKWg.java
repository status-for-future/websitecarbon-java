package websiteco2calculator;

import websiteco2calculator.CO2.Type;

public final class CO2PerKWg {

    static final Double CO2_PER_KWG_GRID = 477.0;
    static final Double CO2_PER_KWG_RENEWABLE = 33.4;
    static final Percentage ENERGY_IN_DATACENTER = Percentage.of(0.1008);
    static final Percentage ENERGY_IN_TRANSMISSION_AND_END_USER = Percentage.of(0.8992);
    private final Double value;

    private CO2PerKWg(Double value) {
        this.value = value;
    }

    public static CO2PerKWg of(KiloWattGrammPerByte kiloWattGrammPerByte) {
        return new CO2PerKWg(kiloWattGrammPerByte.asDouble());
    }

    static CO2PerKWg of(int i) {
        return new CO2PerKWg((double) i);
    }

    public CO2 getGrid() {
        return new CO2(this.value * CO2_PER_KWG_GRID);
    }

    public CO2 getCO2Renewable() {
        double v = (value * CO2PerKWg.ENERGY_IN_DATACENTER.asDouble() * CO2PerKWg.CO2_PER_KWG_RENEWABLE) +
                (value * CO2PerKWg.ENERGY_IN_TRANSMISSION_AND_END_USER.asDouble() * CO2PerKWg.CO2_PER_KWG_GRID);
        return new CO2(v, Type.RENEWABLE);
    }

}
