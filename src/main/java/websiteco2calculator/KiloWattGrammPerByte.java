package websiteco2calculator;

public final class KiloWattGrammPerByte {

    static final double KWG_PER_GB = 1.805;
    static final double KWG_PER_BYTE = KWG_PER_GB / 1073741824.0;
    private final double value;

    private KiloWattGrammPerByte(double value) {
        this.value = value;
    }

    public static KiloWattGrammPerByte of(Bytes value) {
        return new KiloWattGrammPerByte(value.asLong() * KWG_PER_BYTE);
    }

    public double asDouble() {
        return this.value;
    }

    public static KiloWattGrammPerByte of(double value) {
        return new KiloWattGrammPerByte(value);
    }

    public CO2PerKWg toCO2PerKWg() {
        return CO2PerKWg.of(this);
    }

}
