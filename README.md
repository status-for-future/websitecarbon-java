# Website Carbon (java)

This is a website carbon calculator in java.
It's based on the [carbon-API 2.0 by wholegrain](https://gitlab.com/wholegrain/carbon-api-2-0).

## Todos

- [x] Calculate CO<sub>2</sub> based on pageSize
- [ ] Calculate pageSize for a local website (download or estimate?)
- [ ] Make this stuff usable in a build pipeline
